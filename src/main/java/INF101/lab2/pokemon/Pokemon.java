
package INF101.lab2.pokemon;

import java.util.Random;

public class Pokemon {
    String name;
    int healthPoints;
    int maxHealthPoints;
    int strength;


    public Pokemon(String name,int maxHealthPoints, int strength ){
        this.name = name ;
        this.maxHealthPoints = maxHealthPoints;
        this.healthPoints = maxHealthPoints;
        this.strength = strength;
    }


    String getName() {
        return name;
    }


    int getStrength() {
        return strength;
    }


    int getCurrentHP() {
        return healthPoints;
    }
    

    int getMaxHP() {
        return maxHealthPoints;
    }


    boolean isAlive() {
        return healthPoints > 0;
    }


    void damage(int damageTaken) {
        if (damageTaken < 0) {
            return; 
        }
        healthPoints -= damageTaken;
        if (healthPoints < 0) {
            healthPoints = 0;
        }
        System.out.println(name + " takes " + damageTaken + " damage and is left with " + healthPoints + "/" + maxHealthPoints + " HP");
        
    }


    void attack(Pokemon target) {
        Random rand = new Random();
        int damageInflicted = rand.nextInt(this.strength + 1);
        System.out.println(this.name + " attacks " + target.getName() + ".");
        target.damage(damageInflicted);
    
        if (!target.isAlive()) {
            System.out.println(target.getName() + " is defeated by " + this.name + ".");
        }

    }

    @Override
    public String toString() {
        return name + " HP: (" + healthPoints + "/" + maxHealthPoints + ") STR: " + strength;
    }

}
